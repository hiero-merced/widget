'use strict';

import config from '../../config.json';

export function postResponse(operationId, hostname, dataObj, callback) {
  let body = 'data=' + JSON.stringify(dataObj);
  body += '&hostname=' + hostname;

  const route = config.api.url + '/operation/' + operationId + '/response';
  const xhr = new XMLHttpRequest();
  xhr.open('POST', route, true);

  xhr.onreadystatechange = function () {
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
      const response = JSON.parse(xhr.responseText);
      if (callback && typeof callback === 'function') callback(response);
    } else {
      if (callback && typeof callback === 'function') callback('error');
    }
  };

  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.setRequestHeader('Authorization', 'Bearer ' + config.api.token);
  xhr.send(body);
}
