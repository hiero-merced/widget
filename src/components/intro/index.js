'use strict';

import './styles.scss';

import '../brand/styles.scss';
import brand from '../brand/index.html';

export function Intro() {
  const introElement = document.createElement('div');
  introElement.classList.add('intro');
  introElement.innerHTML = brand;

  function displayPartners(partners) {
    const listElement = document.createElement('ul');
    listElement.classList.add('intro__partners', 'partners');

    partners.forEach(function (partner) {
      if (partner.logo) {
        const itemElement = document.createElement('li');
        itemElement.classList.add('partners__item');
        itemElement.innerHTML = partner.logo;
        listElement.appendChild(itemElement);
      }
    });

    introElement.appendChild(listElement);
  }

  function displayTitle(title) {
    const textElement = document.createElement('p');
    textElement.classList.add('intro__title');
    textElement.innerHTML = title;
    introElement.appendChild(textElement);
  }

  function displaySubtitle(subtitle) {
    const textElement = document.createElement('p');
    textElement.classList.add('intro__subtitle');
    textElement.innerHTML = subtitle;
    introElement.appendChild(textElement);
  }

  function displayQuote(quoteObj) {

    const quoteElement = document.createElement('blockquote');
    quoteElement.classList.add('intro__quote');
    quoteElement.innerHTML = quoteObj.text;

    if (quoteObj.cite) quoteElement.setAttribute('cite', quoteObj.cite);
    introElement.appendChild(quoteElement);

    if (quoteObj.author) {
      const widgetQuoteAuthorElement = document.createElement('div');
      widgetQuoteAuthorElement.classList.add('intro__quote-author', 'quote-author');

      const authorAvatarElement = document.createElement('img');
      authorAvatarElement.classList.add('quote-author__avatar');
      authorAvatarElement.setAttribute('src', quoteObj.author.avatar);
      authorAvatarElement.setAttribute('alt', quoteObj.author.name);

      const authorNameElement = document.createElement('p');
      authorNameElement.classList.add('quote-author__name');
      authorNameElement.innerHTML = quoteObj.author.name;

      widgetQuoteAuthorElement.appendChild(authorAvatarElement);
      widgetQuoteAuthorElement.appendChild(authorNameElement);
      introElement.appendChild(widgetQuoteAuthorElement);
    }
  }

  return {
    element: introElement,
    displayPartners: displayPartners,
    displayTitle: displayTitle,
    displaySubtitle: displaySubtitle,
    displayQuote: displayQuote
  };
}
