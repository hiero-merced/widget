'use strict';

import './styles.scss';

export function Spinner() {
  const spinnerElement = document.createElement('p');
  spinnerElement.classList.add('spinner');
  spinnerElement.innerHTML = `
        <span class="spinner__ellipse"></span>
        <span class="spinner__ellipse"></span>
        <span class="spinner__ellipse"></span>
        <span class="spinner__ellipse"></span>
    `;

  return spinnerElement;
}
