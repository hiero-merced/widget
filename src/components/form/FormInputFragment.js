'use strict';

import {id} from '../../utils/id';
import autosize from 'autosize';

export function FormInputFragment(input) {
  const fragment = document.createDocumentFragment();
  const inputId = id();
  let labelElement = null;

  if (input.label && (!input.type || (input.type !== 'submit'))) {
    labelElement = document.createElement('label');
    labelElement.classList.add('form__label');
    labelElement.innerHTML = input.label;
    labelElement.setAttribute('for', inputId);

    if (input.type !== 'checkbox') {
      fragment.appendChild(labelElement);
    }
  }

  switch (input.type) {
    case 'checkbox':
      const checkboxInputElement = document.createElement('input');
      checkboxInputElement.classList.add('form__checkbox');
      checkboxInputElement.setAttribute('type', 'checkbox');
      checkboxInputElement.setAttribute('id', inputId);

      if (input.name) {
        checkboxInputElement.setAttribute('name', input.name);
      }

      if (input.required) {
        checkboxInputElement.setAttribute('required', true);
      }

      fragment.appendChild(checkboxInputElement);

      if (input.label) {
        fragment.appendChild(labelElement);
      }

      break;
    case 'text':
    case 'email':
      const inputWrapperElement = document.createElement('p');
      inputWrapperElement.classList.add('form__input-wrapper');

      const textInputElement = document.createElement('input');
      textInputElement.classList.add('form__input');
      textInputElement.setAttribute('type', input.type);
      textInputElement.setAttribute('id', inputId);

      if (input.name) {
        textInputElement.setAttribute('name', input.name);
      }

      if (input.required) {
        textInputElement.setAttribute('required', true);
      }

      textInputElement.setAttribute('placeholder', ' ');

      inputWrapperElement.appendChild(textInputElement);

      if (input.placeholder) {
        const labelElement = document.createElement('label');
        labelElement.classList.add('form__simili-placeholder');
        labelElement.setAttribute('for', inputId);
        labelElement.innerHTML = input.placeholder;
        inputWrapperElement.appendChild(labelElement);
      }

      fragment.appendChild(inputWrapperElement);

      break;
    case 'textarea':
      const textareaWrapperElement = document.createElement('p');
      textareaWrapperElement.classList.add('form__input-wrapper');

      const textareaElement = document.createElement('textarea');
      textareaElement.classList.add('form__textarea');
      textareaElement.setAttribute('id', inputId);

      if (input.name) {
        textareaElement.setAttribute('name', input.name);
      }

      if (input.required) {
        textareaElement.setAttribute('required', true);
      }

      textareaElement.setAttribute('placeholder', ' ');

      autosize(textareaElement);
      textareaWrapperElement.appendChild(textareaElement);

      if (input.placeholder) {
        const labelElement = document.createElement('label');
        labelElement.classList.add('form__simili-placeholder');
        labelElement.setAttribute('for', inputId);
        labelElement.innerHTML = input.placeholder;
        textareaWrapperElement.appendChild(labelElement);
      }

      fragment.appendChild(textareaWrapperElement);

      break;
    case 'radio':
      const radioFieldsetElement = document.createElement('fieldset');
      radioFieldsetElement.classList.add('form__radio-group');
      input.options.forEach(function (option) {
        const optionInputElement = document.createElement('input');
        const optionInputId = id();
        optionInputElement.classList.add('form__radio');
        optionInputElement.setAttribute('type', 'radio');
        optionInputElement.setAttribute('id', optionInputId);

        if (input.name) {
          optionInputElement.setAttribute('name', input.name);
        }

        if (option.value) {
          optionInputElement.setAttribute('value', option.value);
        }

        if (input.required) {
          optionInputElement.setAttribute('required', true);
        }

        radioFieldsetElement.appendChild(optionInputElement);

        if (option.label) {
          const optionLabelElement = document.createElement('label');
          optionLabelElement.classList.add('form__label');
          optionLabelElement.innerHTML = option.label;
          optionLabelElement.setAttribute('for', optionInputId);
          radioFieldsetElement.appendChild(optionLabelElement);
        }
      });
      fragment.appendChild(radioFieldsetElement);

      break;
    case 'submit':
      const buttonElement = document.createElement('button');
      buttonElement.classList.add('form__button');
      buttonElement.innerHTML = input.label;
      buttonElement.setAttribute('type', 'submit');
      fragment.appendChild(buttonElement);

      break;
    default:
      break;
  }

  return fragment;
}