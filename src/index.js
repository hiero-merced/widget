'use strict';

import config from '../config';

import {extendObject} from './utils/extendObject';

import {Widget} from './components/widget';
import {ShadowDOMWrapper} from './components/ShadowDOMWrapper';
import {IframeWrapper} from './components/IframeWrapper';

import {Session} from './session';

import {isInViewport} from './utils/isInViewport';
import {getUserIP} from './utils/getUserIP';
import {postDataForAudienceAnalysis} from './api/postDataForAudienceAnalysis';

(function (window) {
  const isShadowDomSupported = document.body.attachShadow;

    let params = {};

    let globalObject = window[window['js-widget']];

    let queue = globalObject.q;

    if (queue) {
      for (var i = 0; i < queue.length; i++) {
        if (queue[i][0].toLowerCase() === 'init') {

          params = extendObject(params, queue[i][1]);

          let locations = document.getElementsByClassName(params.class);

          Array.from(locations).forEach((location) => {

            const operation = config.operations.find(function (operation) {
              return operation.id === location.dataset.id;
            });

            if (operation) {
              const widget = Widget(operation);

              if(typeof operation.isTesting === 'undefined') {
                const edgingColor = typeof(operation?.theme?.edging) !== 'undefined' ? operation.theme.edging : null;
                if (isShadowDomSupported) {
                  location.replaceWith(ShadowDOMWrapper(widget, config.mainStylesUrl, edgingColor));
                } else {
                  location.replaceWith(IframeWrapper(widget, config.mainStylesUrl, edgingColor));
                }
              }

              getUserIP(function (ip) {
                Session.hostname = window.location.hostname;
                Session.userPublicIp = ip;

                postDataForAudienceAnalysis(operation.id, 'onDisplay', Session.hostname, Session.userPublicIp);

                let hasBeenInTheViewport = false;
                window.addEventListener('scroll', function (event) {
                  if (isInViewport(widget) && !hasBeenInTheViewport) {
                    postDataForAudienceAnalysis(operation.id, 'onViewingInTheViewport', Session.hostname, Session.userPublicIp);
                    hasBeenInTheViewport = true;
                  }
                }, false);
              });
            }
          });
        }
      }
    }
})(window);
