# handlines

[Try to respect Karma conventions for git commit Msg](http://karma-runner.github.io/2.0/dev/git-commit-msg.html)

## Getting Started

```shell
npm install
```

## Available Scripts

In the project directory, you can run:

#### `npm run start`

#### `npm run build`

#### `npm run sass-lint`
