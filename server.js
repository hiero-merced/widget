//Install express server
var http = require("http");
var auth = require("http-auth");
var static = require('node-static');

var basic = auth.basic({
    realm: "Private area",
    file: __dirname + "/.htpasswd"
});

var file = new static.Server('./dist');


var server = http.createServer(basic.check((request, response) => {
    file.serve(request, response);
}));


server.listen(process.env.PORT || 8080);
console.log("Server is listening");
