const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = () => {
  return [{
    entry: {
      'widget': './src/index.js',
      'main': './site/index.js'
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.html$/,
          use: 'html-loader'
        },
        {
          test: /\.(s*)css$/,
          use: [{
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development'
            }
          }, {
            loader: 'css-loader'
          }, {
            loader: 'sass-loader'
          }]
        }
      ]
    },
    optimization: {
      minimizer: [new UglifyJsPlugin(), new OptimizeCSSAssetsPlugin({})],
    },
    plugins: [
      new MiniCssExtractPlugin({filename: '[name].css'}),
      new CopyWebpackPlugin([
        {from: 'site/index.html'},
        {from: 'site/widget.html'},
        {from: 'demo/', to: 'demo/'},
        {from: 'site/assets/images/', to: 'assets/images/'},
        {from: 'assets/images/', to: 'assets/images/'},
        {from: '.htaccess', to: 'dist/.htaccess'},
        {from: '.htpasswd', to: 'dist/.htpasswd'}
      ])
    ]
  }];
};
